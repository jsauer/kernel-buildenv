FROM debian:stretch-slim
RUN apt-get update && apt-get install -yqq \
	build-essential \
	bc \
	kmod \
	cpio \
	flex \
	libelf-dev \
	libssl-dev \
	openssl \
	&& rm -rf /var/lib/apt/lists/*